plink -pw raspberry pi@<raspberry.pi.ip.address> "sudo pkill -f python2"
plink -pw raspberry pi@<raspberry.pi.ip.address> "sudo rm -fR src/"
plink -pw raspberry pi@<raspberry.pi.ip.address> "mkdir src"
pscp -pw raspberry -C -r src\ pi@<raspberry.pi.ip.address>:/home/pi/pythondev/src
plink -pw raspberry pi@<raspberry.pi.ip.address> "cd pythondev/src; sudo nohup python2 main.py debug > /dev/null & "
