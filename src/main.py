import ptvsd
import sys

# Debugger setup code
sys.stderr = open('../error.log', 'w')
if sys.argv.count > 1 and "debug" in sys.argv:
    ptvsd.enable_attach("my_secret", address = ('0.0.0.0', 3000))
    ptvsd.wait_for_attach()

#
# Actual code starts here
#

import time

while True:
    print("Ready!")
    time.sleep(1)
